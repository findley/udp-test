use std::net::UdpSocket;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn print_usage(program: &str, opts: getopts::Options) {
    let brief = format!("Usage: {} FILE [options]", program);
    print!("{}", opts.usage(&brief));
}

fn main() -> Result<()> {
    let matches = parse_opts()?;
    let local_addr = matches.opt_str("l").unwrap_or("0.0.0.0:1337".into());
    let remote_addr = matches.opt_str("r").unwrap_or("127.0.0.1:1338".into());

    let socket = UdpSocket::bind(&local_addr)?;
    let recv_socket = socket.try_clone()?;
    println!("Opened UDP socket on {}", &local_addr);

    socket.connect(&remote_addr)?;

    std::thread::spawn(move || recv_thread(recv_socket));

    let mut buffer = String::new();
    let stdin = std::io::stdin();
    loop {
        buffer.clear();
        stdin.read_line(&mut buffer)?;
        socket.send(buffer.as_bytes())?;
    }
}

fn recv_thread(socket: UdpSocket) {
    let mut buf: [u8; 25] = [0; 25];
    loop {
        let _amt = socket.recv(&mut buf).unwrap();
        print!("other: {}", std::str::from_utf8(&buf).unwrap().trim());
    }
}

fn parse_opts() -> Result<getopts::Matches> {
    let args: Vec<String> = std::env::args().collect();
    let program = args[0].clone();
    let mut opts = getopts::Options::new();
    opts.optopt(
        "l",
        "localaddr",
        "Local address to open UPD socket on",
        "LOCALE_ADDR",
    );
    opts.optopt(
        "r",
        "remoteaddr",
        "Remote address to connect to",
        "REMOTE_ADDR",
    );
    opts.optflag("h", "help", "print this help menu");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            panic!("{}", f)
        }
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        std::process::exit(0);
    }

    Ok(matches)
}
